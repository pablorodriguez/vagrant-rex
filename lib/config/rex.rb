module VagrantPlugins
  module Rex
    module Config
    class Rex < Vagrant.plugin("2", :config)
      attr_accessor :rexfile

      def initialize
        super

        @rexfile          = UNSET_VALUE
        @task             = UNSET_VALUE
      end

      def finalize!
        super

        @rexfile          = nil if @rexfile == UNSET_VALUE
        @task          = nil if @task == UNSET_VALUE

      end

      def validate(machine)
        errors = _detected_errors

        # Validate that a Rexfile path was provided
        if !rexfile
          errors << I18n.t("vagrant.provisioners.rex.no_rexfile")
        end

        if !task
            errors << I18n.t("vagrant.provisioners.rex.no_task")
        end

        if rexfile
          expanded_path = Pathname.new(rexfile).expand_path(machine.env.root_path)
          if !expanded_path.file?
            errors << I18n.t("vagrant.provisioners.rex.rexfile_path_invalid",
                              :path => expanded_path)
          end
        end
        { "rex provisioner" => errors }
      end
    end
  end
 end
end
