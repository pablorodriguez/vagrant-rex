require "vagrant"

module VagrantPlugins
  module Rex
    class Plugin < Vagrant.plugin("2")
      name "rex"
      description <<-DESC
      Provides support for provisioning your virtual machines with Rex
      DESC

      config(:rex, :provisioner) do
        require_relative "config/rex"
        Config::Rex
      end

      provisioner(:rex) do
        require_relative "provisioner/rex"
        Provisioner::Rex
      end
    end
  end
end
