module VagrantPlugins
  module Rex
      module Provisioner
    class Rex < Vagrant.plugin("2", :provisioner)

      def configure(root_config)
        rexfile_path = Pathname.new(File.dirname(config.rexfile)).expand_path(@machine.env.root_path)
        
        #folder_opts = {}
        #folder_opts[:nfs] = true if config.nfs
        #folder_opts[:owner] = "root" if !folder_opts[:nfs]

        # Share the playbook directory with the guest
        root_config.vm.synced_folder(rexfile_path, config.guest_folder.to_s)
      end

      def provision
        #ssh = @machine.ssh_info

        # Connect with Vagrant user (unless --user or --private-key are overidden by 'raw_arguments')
        #options = %W[--private-key=#{ssh[:private_key_path]} --user=#{ssh[:username]}]
        #options = %W[--connection=local]

        # Joker! Not (yet) supported arguments can be passed this way.
        #options << "#{config.raw_arguments}" if config.raw_arguments


#        options << "--inventory-file=#{self.setup_inventory_file}"
#        options << "--sudo" if config.sudo
#        options << "--sudo-user=#{config.sudo_user}" if config.sudo_user
#        options << "--ask-sudo-pass" if config.ask_sudo_pass

        # Assemble the full ansible-playbook command
        command = (%w(rex) << File.basename(config.rexfile).to_s << config.task).flatten.join(' ')

        @machine.communicate.tap do |comm|
          # Execute it with sudo
          comm.execute(command, sudo: true) do |type, data|
            if [:stderr, :stdout].include?(type)
              @machine.env.ui.info(data, :new_line => false, :prefix => false)
            end
          end
        end
      end

    end
  end
 end
end
